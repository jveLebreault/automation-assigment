
class CNNSearchResultsPage {

    constructor(page) {

        this.page = page;
        this.selectors = {
            SEARCH_RESULTS_CONTAINER: '.cnn-search__results',
            SEARCH_RESULTS_COUNT: '.cnn-search__results-count',
            SEARCH_RESULTS_ERROR_MESSAGE: '.cnn-search__no-results.no-results--returned > h3'
        };
    }

    async hasSearchResults() {
        const resultsString = await this.page.$eval(this.selectors.SEARCH_RESULTS_COUNT, node => node.innerText);
        
        if (resultsString.trim() === "") {
            return 0;
        } else {
            const pattern = new RegExp(/\s\d+\s/);
            return Number(resultsString.match(pattern)[0]);
            
        }
    }

    async getErrorMessage() {
        return await this.page.$eval(this.selectors.SEARCH_RESULTS_ERROR_MESSAGE, node => node.innerText);
    }

}

module.exports = CNNSearchResultsPage;