const CNNSearchResultsPage = require('./CNNSearchResultsPage');

class CNNHomePage {

    constructor(page) {

        this.page = page;
        this.selectors = {
            SEARCH_BUTTON: '#search-button',
            SEARCH_TEXT_BAR: '#search-input-field',
            SUBMIT_BUTTON: '#submit-button'
        };
    }

    async open() {
        return this.page.goto('http://www.cnn.com/');
    }

    async searchForTerm(searchTerm) {
        const searchButton = await this.page.$(this.selectors.SEARCH_BUTTON);
        await searchButton.click();

        const searchField = await this.page.$(this.selectors.SEARCH_TEXT_BAR);
        await searchField.type(searchTerm);

        const submitButton = await this.page.$(this.selectors.SUBMIT_BUTTON);
        await Promise.all([
            submitButton.click(),
            this.page.waitForNavigation({waitUntil: 'networkidle0'})
        ]);

        return Promise.resolve(new CNNSearchResultsPage(this.page));
    }
}

module.exports = CNNHomePage;