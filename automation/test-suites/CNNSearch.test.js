const puppeteer = require('puppeteer');
const CNNHomePage = require('../pages/cnn/CNNHomePage');


jest.setTimeout(30000);

describe('CNN Search test suite', async () => {
    let browser;
    let currentPage;

    beforeAll(async () => {
        browser = await puppeteer.launch({headless: false, defaultViewport: null, args: ['--start-fullscreen']});
        currentPage = await browser.newPage();
    });


    test('Can open CNN Home Page', async () => {
        currentPage = new CNNHomePage(currentPage);

        await currentPage.open();

        let pageTitle = await currentPage.page.title();
        expect(pageTitle).toMatch('CNN');
    });

    test('Can show results when searching for existing term', async () => {
        currentPage = await currentPage.searchForTerm('NFL');

        let searchResults = await currentPage.hasSearchResults();

        expect(searchResults).toBeTruthy();
    });

    test('Go back to Home Page', async () => {
        currentPage = new CNNHomePage(currentPage.page);

        await currentPage.open();
    });

    test('Can show error message when searching for non-existing term', async () => {
        currentPage = await currentPage.searchForTerm('NFLFake');

        let searchResults = await currentPage.hasSearchResults();

        expect(searchResults).toBeFalsy();
        expect(await currentPage.getErrorMessage()).toMatch('NFLFake did not match any documents');
    });

    afterAll(async () => {
        await browser.close();
    })
});